#include <AFMotor.h>

AF_DCMotor motor_s(3);//solenoid connects to M3
AF_DCMotor motor_b(1);//pump motor connects to M1

int num = 0;//input commend

void setup() {
  Serial.begin(9600);
  motor_s.setSpeed(255);
  motor_s.run(RELEASE);//initialize the solenoed
}

void loop() {
  
  if (Serial.available() > 0) //detect is there a new command
  {
    num = Serial.read();//save the command
    //Serial.printin(num);
  }

  if (num == 48) {// command = 0, stop the pump motor
    motor_b.run(RELEASE);
  }
  else if (num == 97) {// command = a, motor mode1
    motor_b.setSpeed(180);
    slow();
  }
  else if (num == 98) {// command = b, motor mode2
    motor_b.setSpeed(200);
    normal();
  }
  else if (num == 99) {//// command = c, motor mode3
    motor_b.setSpeed(255);
    fast();
  }
  else if (num == 100) {// command = d, motor mode4
    motor_b.setSpeed(255);
    com1();
  }
  else if (num == 101) {// command = e, motor mode5
    motor_b.setSpeed(255);
    com2();
  }
  else if (num == 102) {// command = f, release all the air
    release();
  }
}

//motor mode1
void slow() {
  motor_b.run(FORWARD);
  delay(800);
  motor_b.run(RELEASE);
  release();
}

//motor mode2
void normal() {
  motor_b.run(FORWARD);
  delay(300);
  motor_b.run(RELEASE);
  release();
}

//motor mode3
void fast() {
  motor_b.run(FORWARD);
  delay(100);
  motor_b.run(RELEASE);
  release();
}

//motor mode4
void com1() {
  for (int i = 0; i < 5; i++) {
    motor_b.run(FORWARD);
    delay(200);
    motor_b.run(RELEASE);
    delay(50);
  }
  release();
}

//motor mode5
void com2() {
  motor_b.setSpeed(255);
  fast();
  fast();
  motor_b.setSpeed(150);
  slow();
}

//for releasing air
void release() {
  //motor_s.setSpeed(255);
  motor_s.run(FORWARD);
  delay(500);
  motor_s.run(RELEASE);
  delay(50);
}
