class Particle {
  PVector center;
  PVector pos;
  float angle;
  float radius;
  float size;
  float speed;
  float r1_offset, r2_offset;

  Particle(float c_x, float c_y, float s) {
    center = new PVector(c_x, c_y);
    pos = new PVector(random(c_x-50, c_x+50), random(c_y-50, c_y+50));
    float deltaX = center.x - pos.x;
    float deltaY = center.y - pos.y;
    angle = atan2(deltaX, deltaY);
    radius = dist(center.x, center.y, pos.x, pos.y);
    ellipseMode(RADIUS);
    size = random(0.3, 2);
    speed = s;
    r1_offset = random(0.1, 3);
    r2_offset = random(0.1, 3);
  }



  void update(float cx, float cy) {
    center.x = cx;
    center.y = cy;
    pos.x = center.x + cos(angle)*radius*r1_offset;
    pos.y = center.y + sin(angle)*radius*r2_offset;
    angle += PI/speed;
  }


  void display(color c) {
    noStroke();
    fill(c, random(100, 255));
    ellipse(pos.x, pos.y, size, size);
  }
}
