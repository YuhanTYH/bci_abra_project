/**
 * this program is based on oscP5broadcastClient example by andreas schlegel
 * the pupose of the program is visulizing the changing of the Band Power from EEG
 * EEG device hardware: Cyton board
 * EEG software: OpenBCI GUI
 * the program also connects to Arduino to control motor pump
 */

import oscP5.*;
import netP5.*;
import processing.serial.*;


//5 particle system for 5 channels of the band power
ParticleSystem[] p = new ParticleSystem[5] ;

String[] channel = {"DELTA", "THETA", "ALPHA", "BETA", "GAMMA"};

//5 colors for 5 channels of the band power
//in RGB format
color c0 = color(255, 100, 153);//DELTA's color
color c1 = color(255, 255, 0);//THETA's color
color c2 = color(0, 200, 0);//ALPHA's color
color c3 = color(51, 230, 230);//BETA's color
color c4 = color(200, 0, 255);//GAMMA's color
color[] c = {c0, c1, c2, c3, c4};
int color_now = 0;

int count = 0;
int framerate = 30;
int pre_time, cur_time = 0;

float[] datas = new float[5];// save raw data from OpenBCI GUI
float[] val = new float[5];// save modified data for visulization


int delay_time = 0;
int mover_create = 0;

Serial myPort;  // Create object from Serial class


OscP5 oscP5;

/* a NetAddress contains the ip address and port number of a remote location in the network. */
NetAddress myBroadcastLocation;

void setup() {
  size(800, 800);
  frameRate(framerate);


  /* create a new instance of oscP5.
   * 12000 is the port number you are listening for incoming osc messages.
   */
  oscP5 = new OscP5(this, 12000);

  /* create a new NetAddress. a NetAddress is used when sending osc messages
   * with the oscP5.send method.
   */

  /* the address of the osc broadcast server */
  myBroadcastLocation = new NetAddress("127.0.0.1", 12000);


  //change the 0 to a 1 or 2 etc. to match your port
  String portName = Serial.list()[2];

  //communicate with Arduino by Serial port
  myPort = new Serial(this, portName, 9600);

  textSize(20);
}


void draw() {
  background(0);

  //initialize the particle crowds
  if (mover_create == 1) {
    for (int i=0; i<5; i++) {
      p[i] = new ParticleSystem();
    }
  }

  //update the size of the particle crowds
  //update the texts
  if (mover_create > 1) {
    for (int i=0; i<5; i++) {
      p[i].update(val[i], c[i]);
      fill(c[i]);
      text(channel[i]+" : "+nf(datas[i], 0, 4), 50, (height-200)+40*i);
    }
  }
}






/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  /* get and print the address pattern and the typetag of the received OscMessage */
  //println("### received an osc message with addrpattern "+theOscMessage.addrPattern()+" and typetag "+theOscMessage.typetag());
  //theOscMessage.print();

  cur_time = second();

  for (int i=0; i<5; i++) {
    datas[i] = theOscMessage.get(i).floatValue();
    val[i] = datas[i]*100;
  }

  //get the new datas every 2 seconds
  if ( abs(cur_time - pre_time) >2) {
    pre_time = second();

    if (pre_time == 59) {
      pre_time = 0;
    }

    mover_create ++;
    //println(frameCount);
    println(findMax(datas));
    myPort.write(findMax(datas));
    println(datas);
    delay(delay_time);// wait for the latest motor behavior finishes
  }
}


//find the dominated channel (largest value)
char findMax(float[] data) {
  int max_index = 0;
  char result = '0';

  for (int i=0; i<5; i++) {
    if (data[i]>data[max_index]) {
      max_index = i;
    }
  }
  switch(max_index) {
  case 0:
    result = 'a';
    delay_time = 1350;
    break;
  case 1:
    result = 'b';
    delay_time = 850;
    break;
  case 2:
    result = 'c';
    delay_time = 650;
    break;
  case 3:
    result = 'd';
    delay_time = 1800;
    break;
  case 4:
    result = 'e';
    delay_time = 2650;
    break;
  }
  return result;
}
