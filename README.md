# EEG project -- ABRA 2022

<br>

> This protocol documents the programming and hardware setup developed by Yu-Han Tseng according to specifications and concept of artist Hege Tapio.

>The work was executed at Aalto University Fab Lab august 2022 during ABRA-Hub (https://www.abra-hub.net/about/) workshop.

>The protocol is per.august 2022 a first programming and hardware framework for further development of the art installation of Hege Tapio. 

- Hege Tapio: <https://www.tapio.no>

- Yu-han Tseng: <tyhcindy@gmail.com>

<br>

---

This is a documentation about how to get the EEG signal (five channels of band power) and transform them into visual particle effects and different motor behaviors.

## Contents
1. [How to connect **Cyton Board**](https://gitlab.com/YuhanTYH/bci_abra_project#how-to-connect-cyton-board)
2. [How to connect **Arduino and motors**](https://gitlab.com/YuhanTYH/bci_abra_project#how-to-connect-arduino-and-motors)
3. [About visual efffect: **Processing**](https://gitlab.com/YuhanTYH/bci_abra_project#about-visual-efffect-processing)
4. [Connect all the things to gether](https://gitlab.com/YuhanTYH/bci_abra_project#connect-all-together)
5. [Troubleshooting](https://gitlab.com/YuhanTYH/bci_abra_project#troubleshooting)

----

## How to connect **Cyton Board**
### ***Install software: OpenBCI***
1. Download the software from [here](https://openbci.com/downloads)
2. Follow [the steps here](https://docs.openbci.com/Software/OpenBCISoftware/GUIDocs/#installing-the-openbci-gui-as-a-standalone-application) to install it.

### ***Connect the board to the computer***
![USB Dongle](/images/pic_1.jpeg)
1. Insert the USB Dongle into the computer, turning the switch to the side that is closer to the USB port (the back side is "GPIO 6"). The blue LED will turn on.
2. Plug in the 6V battery to the Cyton board.
3. Switch to the "PC" side.
4. Now the Cyton Board is connected to the computer, you can put on [the headband](https://docs.openbci.com/AddOns/Headwear/HeadBand/#headband-cyton-tutorial).



### ***Get data from Cyton Board***
- **Live Data**

  System Control Panel --> CYTON (live) --> AUTO-CONNECT
  
  ![get live data from the headband](/images/pic_3.png)

  - when the live data is processed, the data will be recorded in the folder where you installed the OpenBCI software. (OpenBCI_GUI --> Recordings --> choose the folder with date --> the **.txt** file can be used as PLAYBACK file)

- **Recording Data**

  System Control Penel --> PLAYBACK (from file) --> SELECT OPENBCI PLAYBACK FILE --> select the .txt file --> START SESSION

  ![use the recording data](/images/pic_4.png)



---


## How to connect **Arduino and motors**

### ***Main Component List***
(the links of the components are for reference)
- [Arduino UNO](https://store.arduino.cc/products/arduino-uno-rev3): upload arduino file to this board.
- [L293D V1 Motor Driver Shield](https://protosupplies.com/product/l293d-v1-motor-driver-shield/): for driving motor and the solenoid.
- [6V Mini Air Pump Motor](https://www.ebay.co.uk/itm/262701776253): for inputing the air.
- [6V DC Valves Solenoid](https://www.amazon.com/Aexit-Electrical-Straight-Through-Pressure-Solenoid/dp/B07LFH1BSD): for releasing the air.
- 6V Power Source: power for the motor and the solenoid.

### ***Conect all components***
![connect motors and Arduino](/images/pic_2.jpeg)
1. Install the driver shield on the Arduino UNO board.
2. Connect the wires of the pump motor to **M1** of the driver shield.
3. Connect the wires of the solenoid to **M3** of the driver shield.
4. Remove the jumper, so the 6V power source will only give power to the motors and the solenoid without breaking Arduino UNO.
5. Connect the Arduino UNO to the computer by the cable.

### ***Upload the code to Arduino UNO***
1. Download [**Arduino IDE**](https://www.arduino.cc/en/software).
2. Follow [**this**](https://lastminuteengineers.com/l293d-motor-driver-shield-arduino-tutorial/#:~:text=headers%20to%20it.-,Installing%20AFMotor%20Library,-In%20order%20to) to install library on Arduino IDE to control the motor driver shield.
3. Download [**Arduino file for this project**](/files/motor_pump/) and open it in Arduino IDE.
4. Click the ➡️​ **arrow button** to Upload the programme to the Arduino UNO board. [Here is the tutorial](https://support.arduino.cc/hc/en-us/articles/4733418441116-Upload-a-sketch-in-Arduino-IDE) shows how to upload sketch to Arduino board.

### ***About Arduino code: motor behaviors***

There're 5 mode of motor behavious for 5 channels:

1. DELTA - mode 1: input once and release in slow speed.
2. THETA - mode 2: input once and release in normal speed.
3. ALPHA - mode 3: input once and release in fast speed.
4. BETA - mode 4: input five times and release once as one cycle.
5. GAMMA - mode 5: the combination of mode 3 twice and mode 1 once as a cycle.

There's a more detailed explanation in the code's command.

---

## About visual efffect: **Processing**

### ***Installation tne required library***
1. Download [**Processing**](https://processing.org/download).
2. Download [**oscP5 library**](https://sojamo.de/libraries/oscP5/), unzip the file and copy it.
3. Go to the folder where you installed Processing: Processing --> libraries (if there isn't a folder called "libraries", you can create one) --> past the oscP5 folder here.
4. Download [**Processing file for this project**](/files/openBCI_to_Processing/), open **openBCI_to_Processing.pde** in Processing.



### ***About the particle effect***

![the visual effect of Band Power](/images/video_2.mp4)

- The button-left texts are the raw datas (round to 4 decimal places)from the OpenBCI software.
- Each chennal has a color, you can change the color in the Processing code by RGB format.

```java

  color c0 = color(255, 100, 153);//DELTA's color
  color c1 = color(255, 255, 0);//THETA's color
  color c2 = color(0, 200, 0);//ALPHA's color
  color c3 = color(51, 230, 230);//BETA's color
  color c4 = color(200, 0, 255);//GAMMA's color

```

- Each group of particles responds to a channel's data of Band Power. It moves randomly around the window, but the size of the group is based on the datas.

- Now the program updates the date around every 2 seconds. Not always exactly 2 seconds because the program will wait for the last motor behavior to finish, then send the new data to Processing.



---


## Connect all together
![This vido](/images/video_1.mp4)

shows how to visualize the recording data by Processing. If the Arduino is connected to the computer at the same time, the motor and the solenoid will respond to the data.

If you want to work with live data, choose CYTON (live) at step 3.

1. Connect Arduino UNO (which is unloded the sketch **motor_pump.ino** already) to the computer USB port by the cable.
2. Open **openBCI_to_Processing.pde** in Processing and run the programme (the top-left arrow button), a black window will show up.
3. Open **OpenBCI GUI**, choose the recording file and start.
4. Because we only send out the raw data of Band Power (five channels), set the window show **Band Power** and **Networking** parts to make the window clearer.
5. Networking setting: 
  
    (1) choose Protocol (top-left) dropdrown menu to **OSC**.

    (2) choose the Data Type of Stream 1 to **AvgBandPower**.

    (3) change the Port of Stream to **12000**.

6. Press **Start Data Stream** on the top-left corner, you will see the bar chart starts to react with the data.

7. Press **Start OSC Stream** on the buttom of the Newworking part, the data will start to send to Processing.After around 2 seconds, you should see five groups of particles with different colors show up in the window of Processing. Depending on different computers' capabilities, the visual effect may lag for the first few seconds, and become smoother later.

8. When the visual effect shows in the window of Processing, the data is also sent to Arduino. The motor should start to operate. The motor responds to the channel which has the greatest value.


---

## Troubleshooting

#### Both the motor and the solenoid don't work. Here are possible reasons:

- The batteries run out the power
  
  => change the batteries.
- The setting of the USB port number is wrong
  
  => Find this line in Processing program, try different numbers in the [ ] and rerun the program.

  ```java
    //change the 0 to a 1 or 2 etc. to match your port
    String portName = Serial.list()[2];
  ```

#### The solenoid doesn't work. The air is input into the container but isn't released.

- The batteries run out the power
  
  => change the batteries.





