class ParticleSystem {

  PVector center;
  float xnoise = random(1000);
  float ynoise = random(1000);
  float size;
  int p_num = 800;
  ArrayList<Particle> p = new ArrayList<Particle>();


  ParticleSystem() {
    center = new PVector(random(width), random(height));
    for (int i=0; i<p_num; i++) {
      p.add(new Particle(center.x, center.y, random(20, 120)));
    }
  }

  void update(float size, color p_c) {
    center.x = map(noise(xnoise), 0, 1, 0, width);
    center.y = map(noise(ynoise), 0, 1, 0, height);
    xnoise += 0.004;
    ynoise += 0.004;


    //println(p.size());

    for (int i=0; i<p.size(); i++) {
      if (p.get(i).radius < size) {
        p.get(i).radius += 1;
      } else {
        p.get(i).radius -= 1;
      }

      p.get(i).update(center.x, center.y);
      p.get(i).display(p_c);
   
    }
  }
}
